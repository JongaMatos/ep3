require 'test_helper'

class DoadosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @doado = doados(:one)
  end

  test "should get index" do
    get doados_url
    assert_response :success
  end

  test "should get new" do
    get new_doado_url
    assert_response :success
  end

  test "should create doado" do
    assert_difference('Doado.count') do
      post doados_url, params: { doado: { Data: @doado.Data, Litros: @doado.Litros, Local: @doado.Local, doador_id: @doado.doador_id } }
    end

    assert_redirected_to doado_url(Doado.last)
  end

  test "should show doado" do
    get doado_url(@doado)
    assert_response :success
  end

  test "should get edit" do
    get edit_doado_url(@doado)
    assert_response :success
  end

  test "should update doado" do
    patch doado_url(@doado), params: { doado: { Data: @doado.Data, Litros: @doado.Litros, Local: @doado.Local, doador_id: @doado.doador_id } }
    assert_redirected_to doado_url(@doado)
  end

  test "should destroy doado" do
    assert_difference('Doado.count', -1) do
      delete doado_url(@doado)
    end

    assert_redirected_to doados_url
  end
end
