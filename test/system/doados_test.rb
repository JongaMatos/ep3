require "application_system_test_case"

class DoadosTest < ApplicationSystemTestCase
  setup do
    @doado = doados(:one)
  end

  test "visiting the index" do
    visit doados_url
    assert_selector "h1", text: "Doados"
  end

  test "creating a Doado" do
    visit doados_url
    click_on "New Doado"

    fill_in "Data", with: @doado.Data
    fill_in "Litros", with: @doado.Litros
    fill_in "Local", with: @doado.Local
    fill_in "Doador", with: @doado.doador_id
    click_on "Create Doado"

    assert_text "Doado was successfully created"
    click_on "Back"
  end

  test "updating a Doado" do
    visit doados_url
    click_on "Edit", match: :first

    fill_in "Data", with: @doado.Data
    fill_in "Litros", with: @doado.Litros
    fill_in "Local", with: @doado.Local
    fill_in "Doador", with: @doado.doador_id
    click_on "Update Doado"

    assert_text "Doado was successfully updated"
    click_on "Back"
  end

  test "destroying a Doado" do
    visit doados_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Doado was successfully destroyed"
  end
end
