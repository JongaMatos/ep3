require "application_system_test_case"

class DoadoresTest < ApplicationSystemTestCase
  setup do
    @doador = doadores(:one)
  end

  test "visiting the index" do
    visit doadores_url
    assert_selector "h1", text: "Doadores"
  end

  test "creating a Doador" do
    visit doadores_url
    click_on "New Doador"

    fill_in "Cpf", with: @doador.CPF
    fill_in "Fator rh", with: @doador.Fator_RH
    fill_in "Nome", with: @doador.Nome
    fill_in "Tipo sanguineo", with: @doador.Tipo_Sanguineo
    click_on "Create Doador"

    assert_text "Doador was successfully created"
    click_on "Back"
  end

  test "updating a Doador" do
    visit doadores_url
    click_on "Edit", match: :first

    fill_in "Cpf", with: @doador.CPF
    fill_in "Fator rh", with: @doador.Fator_RH
    fill_in "Nome", with: @doador.Nome
    fill_in "Tipo sanguineo", with: @doador.Tipo_Sanguineo
    click_on "Update Doador"

    assert_text "Doador was successfully updated"
    click_on "Back"
  end

  test "destroying a Doador" do
    visit doadores_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Doador was successfully destroyed"
  end
end
