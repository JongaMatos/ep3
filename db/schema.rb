# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_09_031113) do

  create_table "doadores", force: :cascade do |t|
    t.string "Nome"
    t.string "CPF"
    t.string "Tipo_Sanguineo"
    t.string "Fator_RH"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "doados", force: :cascade do |t|
    t.date "Data"
    t.float "Litros"
    t.string "Local"
    t.integer "doador_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["doador_id"], name: "index_doados_on_doador_id"
  end

  create_table "funcionarios", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "gerente"
    t.index ["email"], name: "index_funcionarios_on_email", unique: true
    t.index ["reset_password_token"], name: "index_funcionarios_on_reset_password_token", unique: true
  end

end
