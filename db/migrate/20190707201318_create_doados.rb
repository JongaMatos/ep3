class CreateDoados < ActiveRecord::Migration[5.2]
  def change
    create_table :doados do |t|
      t.date :Data
      t.float :Litros
      t.string :Local
      t.references :doador, foreign_key: true

      t.timestamps
    end
  end
end
