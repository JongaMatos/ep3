class CreateDoadores < ActiveRecord::Migration[5.2]
  def change
    create_table :doadores do |t|
      t.string :Nome
      t.string :CPF
      t.string :Tipo_Sanguineo
      t.string :Fator_RH

      t.timestamps
    end
  end
end
