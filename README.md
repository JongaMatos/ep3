Ep3-OO
1/2019
João Gabriel de Campos de Matos 18/0042238

Projeto:Site de registro de doaçoes de sangue para uma empresa
Tipos de usuario:
1)Funcionario sem cargo ("Apenas" funcionario)
  Pode registrar doadores e doaçoes,mas não pode excluir e nem editar, e tem acesso limitado a visualizaçoes
2)Funcionario com cargo de gerente
 Pode registrar,editar e excluir doadores e doaçoes.
 Pode tambem visualizar os funcionarios(de ambos os tipos) que se encontram cadastrados.

Doadores:
São registrados por qualquer funcionario
Possuem Nome,CPF,Tipo sanguineo e Fator rh
Podem possuir multiplas doações

Doação(por motivo de dificuldades para corrigir pluralização,no codigo chama-se doado):
Possuem Data,Local e Quantidade de sangue doada em ml.
Nescessariamente pertencem a um doador.
São automaticamente removidas caso o doador associado for deletado.

Control_funcionarios:
Apenas mostra aos gerentes uma tabela com todos os funcionarios(de qualquer tipo) 

Welcome:
Pagina inicial com links para login,logout,cadastro de funcionarios,index de doadores e doados.