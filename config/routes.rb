Rails.application.routes.draw do
  
  devise_for :funcionarios, :controllers => {registrations: 'registrations'}
  get 'controll_funcionarios/index'
  resources :doados
  resources :doadores
  resources :welcome
  resources :controll_funcionarios
  root to:  "welcome#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
