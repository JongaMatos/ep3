class Doador < ApplicationRecord
	has_many :doados 
	validates :Nome , :CPF , :Tipo_Sanguineo, :Fator_RH, presence: true
	validates :CPF , numericality: true , uniqueness: true , length: { is: 11 }
	


end
