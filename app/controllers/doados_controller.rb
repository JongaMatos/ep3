class DoadosController < ApplicationController
  before_action :authenticate_funcionario!
  before_action :set_doado, only: [:show, :edit, :update, :destroy]

  # GET /doados
  # GET /doados.json
  def index
    @doados = Doado.all
  end

  # GET /doados/1
  # GET /doados/1.json
  def show
  end

  # GET /doados/new
  def new
    @doado = Doado.new
  end

  # GET /doados/1/edit
  def edit
  end

  # POST /doados
  # POST /doados.json
  def create
    @doado = Doado.new(doado_params)

    respond_to do |format|
      if @doado.save
        format.html { redirect_to @doado, notice: 'Doação foi adicionada com sucesso.' }
        format.json { render :show, status: :created, location: @doado }
      else
        format.html { render :new }
        format.json { render json: @doado.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /doados/1
  # PATCH/PUT /doados/1.json
  def update
    respond_to do |format|
      if @doado.update(doado_params)
        format.html { redirect_to @doado, notice: 'Doação foi atualizada com sucesso.' }
        format.json { render :show, status: :ok, location: @doado }
      else
        format.html { render :edit }
        format.json { render json: @doado.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /doados/1
  # DELETE /doados/1.json
  def destroy
    @doado.destroy
    respond_to do |format|
      format.html { redirect_to doados_url, notice: 'Doação foi apagada com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_doado
      @doado = Doado.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def doado_params
      params.require(:doado).permit(:Data, :Litros, :Local, :doador_id)
    end
end
